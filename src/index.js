import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();

// handleSearch = () => {
//   const { search } = this.state;
//   const searchLower = search.toLowerCase();
//   const filteredUsers = userList.filter(
//     (user) =>
//       user.name.toLowerCase().includes(searchLower) ||
//       user.phone.includes(search)
//   );

//   this.setState({ users: filteredUsers });
// };
