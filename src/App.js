import React from "react";
import "./App.css";
import UserInfo from "./components/UserInfo";
import userList from "./model/userList";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      phone: "",
      search: "",
      users: userList,
    };
  }

  handleAddUser = () => {
    const { userName, phone, users } = this.state;

    if (!userName.trim() || !phone.trim()) {
      alert("Vui lòng nhập đủ thông tin!");
      return;
    }

    const newUser = {
      name: userName,
      phone: phone,
    };

    const updatedUsers = [...users, newUser];
    updatedUsers.sort((a, b) => a.name.localeCompare(b.name));

    this.setState({
      users: updatedUsers,
      userName: "",
      phone: "",
    });
  };

  handleDeleteDuplicates = () => {
    const { users } = this.state;

    const checkUsers = users.filter((user, index) => {
      const checkIndex = users.findIndex((u) => u.phone === user.phone);
      return checkIndex === index;
    });

    this.setState({ users: checkUsers });
  };

  render() {
    const { userName, phone, search, users } = this.state;

    return (
      <div className="container">
        <div className="form">
          <div className="top">
            <input
              type="text"
              value={userName}
              placeholder="Tên"
              className="inputText"
              onChange={(e) => this.setState({ userName: e.target.value })}
            />
            <input
              type="phone"
              value={phone}
              placeholder="Điện thoại"
              className="inputPhone"
              onChange={(e) => this.setState({ phone: e.target.value })}
            />
            <button className="btnSend" onClick={this.handleAddUser}>
              Thêm
            </button>
          </div>
          <hr />
          <div className="bottom">
            <input
              type="search"
              value={search}
              placeholder="Tìm kiếm"
              className="inputSearch"
              onChange={(e) => this.setState({ search: e.target.value })}
            />
            <button className="btnSearch" onClick={this.handleSearch}>
              Tìm
            </button>
            <button className="btnDelete" onClick={this.handleDeleteDuplicates}>
              Xoá trùng
            </button>
          </div>
          <br />
          <div className="list">
            {users.map((item, index) => {
              return (
                <UserInfo key={index} name={item.name} phone={item.phone} />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
export default App;
