import React from "react";
import style from "../style/style.css";

const UserInfo = (props) => {
  const { name, phone } = props;
  return (
    <>
      <div className="listInfo" style={style}>
        <div className="name"> {name}</div>
        <div className="phone">{phone}</div>
      </div>
    </>
  );
};

export default UserInfo;
